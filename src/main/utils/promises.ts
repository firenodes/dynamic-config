import { ObjectUpdate } from "../types"

import { setValueForKey } from "./object"

function executePromiseAtIndex(
    index: number,
    promise: Promise<any>,
): Promise<[number, any]> {
    return promise.then(
        (val: any): [number, any] => {
            return [index, val]
        },
        (err: any): [number, null] => {
            return [index, null]
        },
    )
}

export function some(promises: Promise<any>[]): Promise<any[]> {
    return new Promise((resolve, reject) => {
        const temp: any[] = []
        let count: number = 0
        promises.forEach((next: Promise<any>, index: number) => {
            executePromiseAtIndex(index, next).then(
                (tuple: [number, any]) => {
                    count++
                    temp[tuple[0]] = tuple[1]
                    if (count === promises.length) {
                        const result = temp.filter((val: any) => val !== null)
                        if (result.length > 0) resolve(result)
                        else
                            reject(
                                new Error(
                                    "All promises completed without success",
                                ),
                            )
                    }
                },
                (err: any) => {
                    count++
                    if (count >= promises.length) {
                        const result = temp.filter((val: any) => val !== null)
                        if (result.length > 0) resolve(result)
                        else
                            reject(
                                new Error(
                                    "All promises completed without success",
                                ),
                            )
                    }
                },
            )
        })
    })
}

/**
 * Given an array of Promises return a new Promise that resolves with the value
 * of the first of the array to resolve, ignoring rejections. The resulting Promise
 * only rejects if all of the Promises reject.
 */
export function race(promises: Promise<any>[]): Promise<any> {
    const count: number = promises.length
    let current: number = 0
    let resolved: boolean = false

    return new Promise((resolve, reject) => {
        promises.forEach((next: Promise<any>) => {
            next.then(
                (val: any) => {
                    if (!resolved) {
                        resolved = true
                        resolve(val)
                    }
                },
                (err: any) => {
                    current++
                    if (!resolved && current === count)
                        reject(
                            new Error("All Promises rejected without success"),
                        )
                },
            )
        })
    })
}

/**
 * The first value in this tuple is the resolved value of the promise
 * The second value is the in-order index of this Promise so that promises are applied in
 * the same order in which they occur in the Record<string, unknown>
 */
type PromiseUpdate = [Record<string, unknown>, number]

/**
 * Recursively traverses an Record<string, unknown>, looking for keys with Promised values and returns a Promise of
 * the Record<string, unknown> with all nested Promises resolved.
 */
export async function valuesForPromises(
    promises: Promise<Record<string, unknown>>[],
): Promise<Record<string, unknown>[]> {
    return Promise.all(
        promises.map(
            (next: Promise<Record<string, unknown>>, index: number) => {
                return resolveAtIndex(next, index)
            },
        ),
    ).then((values: PromiseUpdate[]) => {
        return processValues(values)
    })
}

function processValues(values: PromiseUpdate[]): Record<string, unknown>[] {
    return values
        .sort((a: PromiseUpdate, b: PromiseUpdate) => {
            if (a[1] < b[1]) return -1
            else return 1
        })
        .map((next: PromiseUpdate) => {
            return next[0]
        })
}

function resolveAtIndex(
    promise: Promise<Record<string, unknown>>,
    index: number,
): Promise<PromiseUpdate> {
    return new Promise((resolve, reject) => {
        promise.then(
            (val: Record<string, unknown>) => {
                return resolve([val, index])
            },
            (err: any) => {
                return reject(err)
            },
        )
    })
}

function appendUpdateForObject(
    value: any,
    path: string[],
    updates: ObjectUpdate[],
): void {
    if (value instanceof Promise) updates.push([path, value])
    else if (typeof value === "object")
        collectUnresolvedPromises(value, path, updates)
}

function collectUnresolvedPromises(
    obj: any,
    path: string[] = [],
    updates: ObjectUpdate[] = [],
): ObjectUpdate[] {
    if (Array.isArray(obj)) {
        for (let i = 0; i < obj.length; i++) {
            const value = obj[i]
            const newPath: string[] = [...path, `${i}`]
            appendUpdateForObject(value, newPath, updates)
        }

        return updates
    } else if (obj instanceof Promise) {
        updates.push([path, obj])
        return updates
    } else if (typeof obj === "object") {
        for (const key of Object.keys(obj)) {
            const value = obj[key]
            const newPath: string[] = [...path, key]
            appendUpdateForObject(value, newPath, updates)
        }

        return updates
    } else return []
}

async function handleUnresolved(
    unresolved: ObjectUpdate[],
    base: Record<string, unknown>,
): Promise<Record<string, unknown>> {
    const paths: string[] = unresolved.map((next: ObjectUpdate) =>
        next[0].join("."),
    )
    const promises: Promise<Record<string, unknown>>[] = unresolved.map(
        (next: ObjectUpdate) => next[1],
    )
    const resolvedPromises: Record<string, unknown>[] = await Promise.all(
        promises.map((next: Promise<Record<string, unknown>>) => {
            return next.then((val: Record<string, unknown>) => {
                const nested: ObjectUpdate[] = collectUnresolvedPromises(val)

                if (nested.length > 0) return handleUnresolved(nested, val)
                else return Promise.resolve(val)
            })
        }),
    )

    const newObj: Record<string, unknown> = resolvedPromises.reduce(
        (acc: Record<string, unknown>, next: any, currentIndex: number) => {
            return setValueForKey(paths[currentIndex], next, acc)
        },
        base,
    )

    return newObj
}

export async function resolveObjectPromises(
    obj: Record<string, unknown>,
): Promise<any> {
    const unresolved: ObjectUpdate[] = collectUnresolvedPromises(obj)
    return handleUnresolved(unresolved, obj)
}
