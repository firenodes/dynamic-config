import JsonValidator from "ajv"
import { IObjectSchema, ISchema } from "../types"

const JSON_VALIDATOR: JsonValidator = new JsonValidator()

export function objectMatchesSchema(schema: ISchema, data: unknown): boolean {
    return JSON_VALIDATOR.validate(schema, data) as boolean
}

/**
 * Creates a schema for the given object. The resulting schema is a simple JSON Schema.
 */
export function objectAsSimpleSchema(obj: unknown): ISchema {
    const objType = typeof obj

    if (Array.isArray(obj))
        return {
            type: "array",
            items: objectAsSimpleSchema(obj[0]),
        }
    else if (objType === "object") {
        const schema: IObjectSchema = {
            type: "object",
            properties: {},
            required: [],
        }

        if (obj !== null)
            for (const key of Object.keys(obj as Record<string, unknown>)) {
                const propSchema = objectAsSimpleSchema(
                    (obj as Record<string, unknown>)[key] as Record<
                        string,
                        unknown
                    >,
                )
                schema.properties![key] = propSchema as Record<string, unknown>

                if (
                    schema.required !== undefined &&
                    (propSchema as any).type !== "undefined"
                )
                    schema.required.push(key)
            }

        return schema
    } else if (objType !== "function" && objType !== "symbol")
        return {
            type: objType,
        } as ISchema
    else throw new Error(`Type[${objType}] cannot be encoded to JSON`)
}
