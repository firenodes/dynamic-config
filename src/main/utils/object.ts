import { isNothing, isObject, isPrimitive, splitKey } from "./basic"

import { objectAsSimpleSchema, objectMatchesSchema } from "./json"

import { ISchema } from "../types"

/**
 * Map over all keyed elements in an object, running over leafs first and recursing up.
 *
 * @param obj
 * @param mapping
 */
export function deepMap(
    mapping: (val: unknown, path: string) => unknown,
    obj: unknown,
    path: string = "",
): unknown {
    if (isNothing(obj) || isPrimitive(obj)) return obj
    else if (Array.isArray(obj)) {
        const newObj: unknown[] = []
        for (let i = 0; i < obj.length; i++) {
            const currentPath: string =
                path.length > 0 ? `${path}[${i}]` : `[${i}]`
            const value = obj[i]
            if (isNothing(value)) newObj[i] = value
            else if (isPrimitive(value)) newObj[i] = mapping(value, currentPath)
            else {
                newObj[i] = mapping(value, currentPath)
                newObj[i] = deepMap(
                    mapping,
                    newObj[i] as Record<string, unknown>,
                    currentPath,
                )
                newObj[i] = mapping(newObj[i], currentPath)
            }
        }

        return newObj
    } else {
        const newObj: Record<string, unknown> = {}
        for (const key in obj as Record<string, unknown>)
            if ((obj as Record<string, unknown>).hasOwnProperty(key)) {
                const currentPath: string =
                    path.length > 0 ? `${path}.${key}` : `${key}`
                const value = (obj as Record<string, never>)[key]
                if (isNothing(value)) newObj[key] = value
                else if (isPrimitive(value))
                    newObj[key] = mapping(value, currentPath)
                else {
                    newObj[key] = mapping(value, currentPath)
                    newObj[key] = deepMap(
                        mapping,
                        newObj[key] as Record<string, unknown>,
                        currentPath,
                    )
                    newObj[key] = mapping(newObj[key], currentPath)
                }
            }

        return newObj
    }
}

export function getValueForKey<T>(
    key: string,
    obj: Record<string, unknown>,
): T | null {
    if (isPrimitive(obj) || isNothing(obj)) return null
    else {
        const parts: string[] = splitKey(key)

        if (parts.length > 1) {
            const [head, ...tail] = parts
            const sub: unknown = obj[head]

            if (!isPrimitive(sub))
                return getValueForKey<T>(
                    tail.join("."),
                    sub as Record<string, unknown>,
                )
            else return null
        } else if (obj[parts[0]] !== undefined) return obj[parts[0]] as T
        else return null
    }
}

export function setValueForKey<T>(
    key: string,
    value: unknown,
    oldObj: Record<string, unknown>,
): T {
    if (typeof key !== "string")
        throw new Error("Property to set must be a string")
    else if (oldObj === null)
        throw new Error(`Cannot set value on null type at key: ${key}`)
    else if (key === "") return value as T
    else {
        const newObj = (Array.isArray(oldObj) ? [] : {}) as Record<
            string,
            unknown
        >
        const [head, ...tail] = splitKey(key)

        const props: string[] = Object.keys(oldObj)

        for (const prop of props)
            if (prop === head)
                if (tail.length > 0) {
                    const nextObj = oldObj[prop] || {}
                    newObj[prop] = setValueForKey(
                        tail.join("."),
                        value,
                        nextObj as Record<string, unknown>,
                    )
                } else newObj[prop] = value
            else newObj[prop] = oldObj[prop]

        return newObj as T
    }
}

function max(...nums: number[]): number {
    return nums.reduce((acc: number, next: number): number => {
        if (next > acc) return next
        else return acc
    })
}

export function overlayArrays<T>(base: T[], update: T[]): T[] {
    const newArray: unknown[] = []
    const baseLen: number = base.length
    const updateLen: number = update.length
    const len: number = max(baseLen, updateLen)

    for (let i = 0; i < len; i++) {
        const baseValue = base[i]
        const updateValue = update[i]

        if (Array.isArray(baseValue) && Array.isArray(updateValue))
            newArray[i] = overlayArrays(baseValue, updateValue)
        else if (isObject(baseValue) && isObject(updateValue))
            newArray[i] = overlay(
                baseValue as Record<string, unknown>,
                updateValue as Record<string, unknown>,
            )
        else if (updateValue !== undefined) newArray[i] = updateValue
        else newArray[i] = baseValue
    }

    return newArray as T[]
}

export function overlay<
    Base extends Record<string, unknown>,
    Update extends Record<string, unknown>
>(base: Base, update: Update): Base & Update {
    const newObj: Record<string, unknown> = {}
    const baseKeys: string[] = Object.keys(base)
    const updateKeys: string[] = Object.keys(update)

    for (const key of updateKeys)
        if (baseKeys.indexOf(key) === -1) baseKeys.push(key)

    for (const key of baseKeys)
        if (base.hasOwnProperty(key) || update.hasOwnProperty(key)) {
            const baseValue: unknown = base[key]
            const updateValue: unknown = update[key]

            if (Array.isArray(baseValue) && Array.isArray(updateValue))
                newObj[key] = overlayArrays(baseValue, updateValue)
            else if (isObject(baseValue) && isObject(updateValue))
                newObj[key] = overlay(
                    baseValue as Record<string, unknown>,
                    updateValue as Record<string, unknown>,
                )
            else if (updateValue !== undefined) newObj[key] = updateValue
            else newObj[key] = baseValue
        }

    return newObj as Base & Update
}

export function overlayObjects(...configs: unknown[]): unknown {
    return configs.reduce(
        (acc: Record<string, unknown>, next: Record<string, unknown>) => {
            return overlay(acc, next)
        },
        {},
    )
}

export function arraysAreEqual(arr1: unknown[], arr2: unknown[]): boolean {
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) return arr1 === arr2
    else if (arr1.length !== arr2.length) return false
    else {
        for (let i = 0; i < arr1.length; i++) {
            const item1: unknown = arr1[i]
            const item2: unknown = arr2[i]

            if (Array.isArray(item1) && Array.isArray(item2)) {
                if (!arraysAreEqual(item1, item2)) return false
            } else if (isObject(item1) && isObject(item2)) {
                if (!objectsAreEqual(item1, item2)) return false
            } else if (item1 !== item2) return false
        }

        return true
    }
}

export function objectsAreEqual(obj1: unknown, obj2: unknown): boolean {
    if (!isObject(obj1) || !isObject(obj2)) return obj1 === obj2
    else {
        const keys1 = Object.keys(obj1)
        const keys2 = Object.keys(obj2)

        if (!arraysAreEqual(keys1, keys2)) return false
        else {
            for (const key of keys1) {
                const value1: unknown = (obj1 as Record<string, unknown>)[key]
                const value2: unknown = (obj2 as Record<string, unknown>)[key]

                if (isObject(value1) && isObject(value2)) {
                    if (!objectsAreEqual(value1, value2)) return false
                } else if (Array.isArray(value1) && Array.isArray(value2)) {
                    if (!arraysAreEqual(value1, value2)) return false
                } else if (value1 !== value2) return false
            }

            return true
        }
    }
}

export function objectHasShape(
    shape: Record<string, unknown>,
    obj: Record<string, unknown>,
): boolean
export function objectHasShape(
    shape: Record<string, unknown>,
): (obj: Record<string, unknown>) => boolean
export function objectHasShape(...args: unknown[]): unknown {
    const targetSchema: ISchema = objectAsSimpleSchema(
        args[0] as Record<string, unknown>,
    )

    if (args.length === 2)
        return objectMatchesSchema(
            targetSchema as Record<string, unknown>,
            args[1],
        )
    else
        return (obj: Record<string, unknown>): boolean => {
            return objectMatchesSchema(
                targetSchema as Record<string, unknown>,
                obj,
            )
        }
}
